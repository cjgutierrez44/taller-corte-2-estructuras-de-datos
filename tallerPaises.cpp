//CHRISTHIAN GUTIERREZ 67000544


#include <iostream>

using namespace std;

struct ciudad{
	string nombre;
	long habitantes;
	ciudad* siguiente;
};

struct pais{
	string nombre;
	long habitantes;
	long extension;
	pais* siguiente;
	ciudad* cabCiudades;
};

void insertarCiudad(ciudad* &cabecera, string nombre, long habitantes){
	ciudad* nuevo = new ciudad;
	nuevo->nombre = nombre;
	nuevo->habitantes = habitantes;
	ciudad* ultimo = cabecera;
	nuevo->siguiente = NULL;
	if(cabecera != NULL){
		ciudad* ultimo = cabecera;
		while(ultimo->siguiente != NULL){
			ultimo = ultimo->siguiente;
		}
		ultimo->siguiente = nuevo;
	}else{
		cabecera = nuevo;
	}
}

void insertarPais(pais* &cabecera,string nombre, long habitantes, long extension){
	pais* nuevo = new pais;
	nuevo->nombre = nombre;
	nuevo->habitantes = habitantes;
	nuevo->extension = extension;
	nuevo->cabCiudades = NULL;

	nuevo->siguiente = NULL;
	if(cabecera != NULL){
		pais* ultimo = cabecera;
		while(ultimo->siguiente != NULL){
			ultimo = ultimo->siguiente;
		}
		ultimo->siguiente = nuevo;
	}else{
		cabecera = nuevo;
	}
}
void imprimirCiudades(ciudad* cabecera){
	ciudad* aux = cabecera;
	while(aux != NULL){
		cout << aux->nombre << ":: "<< "Habitantes: "<< aux->habitantes << " ->";
		aux = aux->siguiente;
	}
	cout<<endl;
}
void imprimirTodo(pais* cabecera, bool todo){
	pais* aux = cabecera;
	while(aux != NULL){
		if(todo == true){
			cout << "= Nombre: " << aux->nombre <<"\n\tHabitantes: " << aux->habitantes << "\n\tExtension: " << aux->extension << ":\n\t";
				imprimirCiudades(aux->cabCiudades);
		}else{
				cout << "= Nombre: " << aux->nombre <<"\n\tHabitantes: " << aux->habitantes << "\n\tExtension: " << aux->extension << "\n";
		}
		aux = aux->siguiente;
	}
	cout<<endl;
}



pais* getCabecera(pais* cabecera, string buscar){
	pais* aux = cabecera;
	ciudad* cab = cabecera->cabCiudades;
	while(aux->nombre != buscar){
		aux = aux->siguiente;
	}
	return aux;
}

void modificarCantidadDeHabitantesCiudad(ciudad* &cabecera, string nombre, long habitantes){
	ciudad* aux = cabecera;
	while(aux->nombre != nombre){
		aux = aux->siguiente;
	}
	aux->habitantes = habitantes;
}

int cantidadDeHabitantesCiudadPais(pais* cabecera, string nombreBuscar){
	int cant = 0; 
	ciudad* aux = getCabecera(cabecera, nombreBuscar)->cabCiudades;
	while(aux != NULL){
		cant += aux->habitantes;
		aux = aux->siguiente;
	}
	return cant;
}

ciudad* ciudadConMasHabitantes(pais* cabecera){
	ciudad* mayor = cabecera->cabCiudades;
	pais* aux = cabecera;
	while(aux != NULL){
		ciudad* aux1 = aux->cabCiudades;
		while(aux1 != NULL){
			if(aux1->habitantes > mayor->habitantes){
				mayor = aux1;
			}
			aux1 = aux1->siguiente;
		}
		aux = aux->siguiente;
	}
	return mayor;
}
double calcularDensidad(double hab, double ext){
	double res = hab/ext;
	return res;
}

pais* determinarPaisConMasDensidadP(pais* &cabecera){
	pais* mayor = cabecera;
	pais* aux = cabecera;

	while(aux != NULL){
		double habAux, habMayor, extAux,extMayor, densidad1,densidad2;
		habAux = aux->habitantes;
		habMayor = mayor->habitantes;
		extAux = aux->extension;
		extMayor = mayor->extension;
		densidad1 = habAux/extAux;
		densidad2 = habMayor/extMayor;
		if(densidad1>densidad2){	
			mayor = aux;
		}
		aux = aux->siguiente;
	}
	return mayor;
}

void ciudadesMasN(pais* cabecera, long N){
	pais* aux = cabecera;
	while(aux != NULL){
		ciudad* aux1 = aux->cabCiudades;
		while(aux1 != NULL){
			if(aux1->habitantes > N){
				cout<< aux1->nombre<<endl;
			}
			aux1 = aux1->siguiente;
		}
		aux = aux->siguiente;
	}
}

bool validarExistenciaPais(pais* cabecera, string nombre){
	pais* aux = cabecera;
	while(aux != NULL){
		if(nombre == aux->nombre){
			return true;
		}
		aux = aux->siguiente;
	}
		return false;
}

bool validarExistenciaCiudad(pais* cabecera, string nombre){
	pais* aux = cabecera;
	while(aux != NULL){
		ciudad* aux2 = aux->cabCiudades;
		while(aux2 != NULL){
			if(nombre == aux2->nombre){
				return true;
			}
			aux2 = aux2->siguiente;
		}
		aux2 = aux->cabCiudades;
		aux = aux->siguiente;
	}
	return false;
}

int main(){
	
	pais* cabecera = NULL;
	pais* buscar;
	int op, cantPaises, cantCiudades;
	
	string nombre;
	long habitantes;
	long extension;			
	
	while(op!=11){
		cout<<"Menu:\n";
		cout<<"1. Registrar paises.\n";
		cout<<"2. Registrar ciudades.\n";
		cout<<"3. Imprimir lista de paises.\n";
		cout<<"4. Imprimir lista de paises con sus ciudades.\n";
		cout<<"5. Imprimir la lista de ciudades de un pais.\n";
		cout<<"6. Modificar la cantidad de habitantes de una ciudad.(1. taller)\n";
		cout<<"7. Mostrar el total de habitantes de las ciudades de un pais.(2. Taller)\n";
		cout<<"8. Determinar la ciudad con mas habitantes.(3.Taller)\n";
		cout<<"9. Determianr el pais con mayor densidad poblacional.(4. Taller)\n";
		cout<<"10. Mostrar las ciudades con mas de N habitantes. (5. Taller)\n";
		cout<<"11. Salir (6. Taller)\n";
		cout<<"Seleccione una opcion:";
		cin>>op;
		
		switch(op){
			case 1: 
				cout<<"Escriba la cantidad de paises que quiere registrar: ";
				cin>>cantPaises;
				for(int i=0; i<cantPaises; i++){
					cout<<"Escriba el nombre del pais "<<(i+1)<<": ";
					cin>>nombre;
					cout<<"Escriba la cantidad de habitantes del pais "<<(i+1)<<": ";
					cin>>habitantes;
					cout<<"Escriba la extension del pais "<<(i+1)<<": ";
					cin>>extension;
					insertarPais(cabecera, nombre, habitantes, extension);
				}
			break;
			case 2:
				do{
					cout<<"Escriba el nombre del pais al que le quiere registrar ciudades: ";
					cin>>nombre;
					if(validarExistenciaPais(cabecera,nombre) != true){
						cout<<"No se encontro el pais " << nombre << endl;
					}
				}while(validarExistenciaPais(cabecera,nombre) != true);
				

				buscar = getCabecera(cabecera,nombre);
				cout<<"Escriba la cantidad de ciudades que quiere registrar en " << nombre << ": ";
				cin>>cantCiudades;
				for(int i=0; i<cantCiudades; i++){
					cout<<"Escriba el nombre de la ciudad " << (i+1) << ": ";
					cin>>nombre;
					cout<<"Escriba la cantidad de habitantes de la ciudad " << (i+1) << ": ";
					cin>>habitantes;
					insertarCiudad(buscar->cabCiudades, nombre, habitantes);
				}
			break;
			case 3:
				imprimirTodo(cabecera,false);
			break;
			case 4:
				imprimirTodo(cabecera,true);
			break;
			case 5:
				do{
					cout<<"Escriba el nombre del pais del que quiere imprimir sus ciudades: ";
					cin>>nombre;
					if(validarExistenciaPais(cabecera,nombre) == true){
						buscar = getCabecera(cabecera,nombre);
						imprimirCiudades(buscar->cabCiudades);
					}else{
						cout<<"No se encontro el pais " << nombre << endl; 
					}
				}while(validarExistenciaPais(cabecera,nombre) != true);
				
			
			break;
			case 6:
				
				do{
					cout<<"Escriba el nombre del pais del que quiere modificar la cantidad de habitantes de una de sus ciudades: ";
					cin>>nombre;
					if(validarExistenciaPais(cabecera, nombre) != true){
						cout<<"No se encontro el pais " << nombre << endl;
					}
				}while(validarExistenciaPais(cabecera, nombre) != true);
				
				buscar = getCabecera(cabecera,nombre);
				do{
					cout<<"Escriba el nombre de la ciudad de la que quiere modificar sus habitantes: ";
					cin>>nombre;
					if(validarExistenciaCiudad(cabecera, nombre) != true){
						cout<<"No se encontro la ciudad " << nombre << endl; 
					}
				}while(validarExistenciaCiudad(cabecera, nombre) != true);
				cout<<"Escriba la nueva cantidad de habitantes: ";
				cin>>habitantes;
				modificarCantidadDeHabitantesCiudad(buscar->cabCiudades, nombre, habitantes);
			break;
			case 7:
				do{
					cout<<"Escriba el nombre del pais del que quiere ver la cantidad de habitantes en sus ciudades: ";
					cin>>nombre;
					if(validarExistenciaPais(cabecera, nombre) != true){
						cout<<"No se encontro el pais " << nombre << endl;
					}
				}while(validarExistenciaPais(cabecera, nombre) != true);
			
				cout<<nombre << " tiene " << cantidadDeHabitantesCiudadPais(cabecera, nombre) << " habitantes en sus ciudades registradas.\n";
			break;
			case 8:
				cout<<"La ciudad con mas habitantes es: " << ciudadConMasHabitantes(cabecera)->nombre << endl;
			break;
			case 9:
				cout<<"El pais con mayor densidad poblacional es: "<< determinarPaisConMasDensidadP(cabecera)->nombre << endl;
			break;
			case 10:
				cout<<"Escriba la cantidad N de habitantes: ";
				cin>>habitantes;
				cout<<"las ciudades con mas de " << habitantes << " son: \n";
				ciudadesMasN(cabecera, habitantes);
				
			break;
			case 11:
				cout<<"Salida.\n";
			break;
			default:
			cout<<"Opcion no valdia.\n";
			break;
		}
	}
	
	return 0;
}












